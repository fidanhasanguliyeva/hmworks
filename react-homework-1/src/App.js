  import React from 'react';
  import './App.css';
  import {MainPage} from './components/MainPage'
  import './style.scss'  
  function App() {
    return (
      <div className="App">
        <MainPage/>
      </div>
    );
  }

  export default App;
