import React, { Component,useState } from 'react';
import {ModalWindow} from './ModalWindow'
import {Button} from './Buttons'

export class MainPage extends Component {
    constructor(){
        super();
        this.state = {
            firstIsActive: false,
            secondIsActive: false
        }
    }
    setFirstActive = () => {
        this.setState({firstIsActive: !this.state.firstIsActive})
 
    }
    setSecondActive = () => {
        this.setState({secondIsActive:!this.state.secondIsActive})
 
    }
    
    render(){
     
    return (
        <div className='main'>
        <div onClick={this.setFirstActive}>
        {this.state.firstIsActive ? <ModalWindow
              header="Do you want to delete this file? " 
              closeIcon={true}
              color = "#d44637"
              toggle={this.setFirstActive}
              text="Once you delete this file, it won’t be possible to undo this action. 
               Are you sure you want to delete it?"
               action ={[ <Button 
                        text="Ok" 
                        backgroundColor="#b3382c" 
                        onClick={this.setFirstActive}/>,
                        <Button 
                        text="Cancel" 
                        backgroundColor="#b3382c" 
                        onClick={this.setFirstActive}/>]} /> : null}</div>
                                                           

          <div onClick={this.setSecondActive}>
            {this.state.secondIsActive ? <ModalWindow
             header="Do you confirm? " 
             toggle={this.setSecondActive}
             backgroundColor="#d44637"
             closeIcon={false}
             text="Hello World"
             action ={ <Button text="Ok" 
                        backgroundColor="#d44637" 
                        onClick={this.setSecondActive}/>}/> : null }</div>
                 
            
        <Button text="Open first modal window" backgroundColor="#1C66F5" onClick={this.setFirstActive}/>
        <Button text="Open second modal window" backgroundColor="#2cb331" onClick={this.setSecondActive}/>
            
            
        </div>
    )
}
}   