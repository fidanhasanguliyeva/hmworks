import React, { Component } from "react";
import PropTypes from 'prop-types';

export class ModalWindow extends Component {
    
 
  render() {
  
    return (
        <div className="background">
          <div  className="modal-window" onClick={(e) => {e.stopPropagation();}}>
            <div style={{backgroundColor: this.props.backgroundColor}} className="header-container">
              <header className="header" >{this.props.header}
              </header>
            {this.props.closeIcon &&  <button  className="close-btn" onClick={this.props.toggle} className="close-btn">x</button>}
            </div>
            <p className="maintext">{this.props.text}</p>
            {this.props.action}

      </div>
      </div>
    );
  }
}
ModalWindow.propTypes = {
  backgroundColor: PropTypes.string,
  header: PropTypes.string,
  closeIcon: PropTypes.bool,
  toggle: PropTypes.func,
  text: PropTypes.string,
  action: PropTypes.array
}
