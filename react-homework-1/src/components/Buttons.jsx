import React, { Component } from 'react';
import PropTypes from 'prop-types';


 export class Button extends Component {
 
     render() {
     
         return (
            <button 
            className="btn" 
            style={{backgroundColor: this.props.backgroundColor}} 
            onClick={this.props.onClick}>
                {this.props.text}
            </button> )
      
                     
    }
    

}

Button.propTypes = {
    text: PropTypes.string,
    backgroundColor:PropTypes.string,
    onClick:PropTypes.func
}