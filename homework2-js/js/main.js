let firstNumber;
let secondNumber;
do {
  firstNumber = +prompt("Enter first number:", 8);
  secondNumber = +prompt("Enter second number:", 4);
} while (isNaN(firstNumber) || isNaN(secondNumber));
let operator = prompt("Enter operator:");

console.log(Calculator(firstNumber, secondNumber, operator));

function Calculator(number1, number2, operator) {
  let result = 0;
  switch (operator) {
    case "+":
      result = number1 + number2;
      break;
    case "-":
      result = number1 - number2;
      break;
    case "*":
      result = number1 * number2;
      break;
    case "/":
      result = number1 / number2;
      break;
    default:
      console.log("Invalid operator");
      break;
  }
  return result;
}
