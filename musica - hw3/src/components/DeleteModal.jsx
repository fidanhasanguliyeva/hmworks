import React from "react";
import { useState } from "react";

export const DeleteModal = props => {
 
  console.log("deletemidal");
  const {
  
    header,
    closeIcon,
    toggle,
    text,
    okHandle,
    cancelHandle
  } = props;
  return (
    
    <div className="background">
      <div
        className="modal-window"
        onClick={e => {
          e.stopPropagation();
        }}
      >
        <div className="modal-header-container">
          <header className="modal-header">{header}</header>
          {closeIcon && (
            <button
              className="close-btn"
              onClick={toggle}
              className="close-btn"
            >
              x
            </button>
          )}
        </div>
        <p className="maintext">{text}</p>
        <div className="button-container">
          <button onClick={okHandle}>OK</button>
          <button onClick={cancelHandle}>Cancel</button>
        </div>
      </div>
    </div>
  );
};

// DeleteModal.propTypes = {
//   backgroundColor: PropTypes.string,
//   header: PropTypes.string,
//   closeIcon: PropTypes.bool,
//   toggle: PropTypes.func,
//   text: PropTypes.string,
//   action: PropTypes.array
// }
