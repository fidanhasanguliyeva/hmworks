import React from "react";

import "../style.sass";
import {NavLink} from 'react-router-dom'
 
export const Header = () =>  {
 

    return (
      <header className="main-header">
        <div className="header-first-line ">
          <div className="container">
            <div className="header-container">
              <div className="icons">
                <img
                  src={process.env.PUBLIC_URL + "/images/Icons/facebook.png"}
                  alt="facebook"
                ></img>
                <img
                  src={process.env.PUBLIC_URL + "/images/Icons/dribble.png"}
                  alt="dribble"
                ></img>
                <img
                  src={process.env.PUBLIC_URL + "/images/Icons/twitter.png"}
                  alt="twitter"
                ></img>
                <img
                  src={process.env.PUBLIC_URL + "/images/Icons/mail.png"}
                  alt="mail"
                ></img>
                <img
                  src={process.env.PUBLIC_URL + "/images/Icons/vimeo.png"}
                  alt="vimeo"
                ></img>
              </div>
              <div className="first-line-login-cart">
                <a className="login-register">Login / Register</a>
                <button className="cart-btn">
                  <img
                    src={process.env.PUBLIC_URL + "/images/Icons/card.png"}
                  ></img>
                  Cart
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="header-second-line">
            <img src={process.env.PUBLIC_URL + "/images/logo.png"} alt="" />
            <NavLink to='/'>Home</NavLink>
            <NavLink to='/cart'>Cart</NavLink>
            <NavLink to='/fav'>Favourite</NavLink>
          </div>
        </div>
       
      </header>
    );
  }

