import React from "react";



export const ProductCard = props => {
  
  const { src, title, artist,rate, description, price, handleClick } = props;
  return (
    <div>
      <div className="about">
        <img src={src} alt="" />
        <div className="product-container">
          <div className="title">
            {title}
            <span>
              <i> {artist}</i>
            </span>
          </div>
          {rate}
          
        </div>
        <p className="product-container">{description}</p>
        <div className="price-cart-btn product-container">
          <span>${price}</span>{" "}
          <button onClick={handleClick}>ADD TO CART</button>
        </div>
      </div>
    </div>
  );
};
