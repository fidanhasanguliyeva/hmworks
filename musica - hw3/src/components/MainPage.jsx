import React from "react";
import "../style.sass";
import { Header } from "./Header";
import { ListOfProducts } from "./ListOfProducts";
import { Cart, Favourite } from "./pages/index";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";


export const Main = props => {

  

  return (
    <div>
      <Router>
        <Header />
        <Switch>
          <Route path="/" exact component={() => <ListOfProducts />} />
          <Route
            path="/cart"
            component={() => <Cart />}
          />
          <Route path="/fav" component={Favourite} />
        </Switch>
      </Router>
    </div>
  );
};
