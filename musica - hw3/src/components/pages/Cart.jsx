import React, { useState, useEffect } from "react";
import { DeleteModal } from "../DeleteModal";

export const Cart = props => {
  const [productCart, setCart] = useState([]);
  const [active, setActive] = useState(false);
  const [toBeDeleted, setToBeDeleted] = useState();

  console.log(productCart, "cart comp");
  console.log(active, "modal window");
  const getData = () => {
    if (!!localStorage.length) {
      setCart([...JSON.parse(localStorage.getItem("cart"))]);
    }
  };

  useEffect(() => {
    getData();
  }, []);

  useEffect(() => {
    localStorage.setItem("Product cart", JSON.stringify(...productCart));
    return () => console.log("Saved to Product Card");
  });
  const handleCLick = (id, count) => {
    setToBeDeleted({ id, count });
    setActive(true);
  };
  const removeFromCart = ({ id, count }) => {
    if (count > 1) {
      setCart(productCart =>
        productCart.map(item => {
          if (item.id === id) {
            return {
              ...item,
              count: item.count - 1
            };
          }
          return item;
        })
      );
    } else {
      setCart(productCart => productCart.filter(product => product.id !== id));
    }
    setActive(false);
    setToBeDeleted({});
  };

  return (
    <div>
      {active && (
        <DeleteModal
          key={1}
          header="Do you want to delete product from card?"
          text="Are you sure?"
          closeIcon={true}
          toggle={() => setActive(false)}
          okHandle={() => removeFromCart({ ...toBeDeleted })}
          cancelHandle={() => setActive(false)}
        />
      )}
      <div className="checkout-card container">
        {productCart.map(({ id, title, cost, count }) => (
          <div key={id} className="checkout-item">
            <h4>{title}</h4>
            <p>Price: ${cost}</p>
            <p>Count:  {count}</p>

            <button onClick={() => handleCLick(id, count)}>X</button>
          </div>
        ))}
        <div className="total">
           <b>Total Delivery cost:</b> 
           <div className="total-price">
             ${ productCart.reduce((total, { cost, count }) =>(parseFloat(total) + cost * count).toFixed(2), 0)}
             </div>
             </div>
            
      </div>
    </div>
  );
};
