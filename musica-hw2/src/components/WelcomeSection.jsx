import React, { Component } from 'react';
import '../style.sass'
export class MainSection extends Component {
    render(){
return(
    <div>
    <section className="welcome-to-musica">
      <div className="container">
        <h1 className="welcome-heading">
          WELCOME TO <span>MUSICA,</span> CHECK OUR LATEST ALBUMS
        </h1>
        <div className="welcome-to-musica-content">
          <div className="options">
            <h3>
              <img
                src={
                  process.env.PUBLIC_URL +
                  "/images/Icons/feature-icon/cd.png"
                }
                alt=""
              />
              CHECK OUR CD COLLECTION
            </h3>
            <p>
              Donec pede justo, fringilla vel, al, vulputate eget, arcu. In{" "}
            </p>
          </div>
          <div className="options">
            <h3>
              <img
                src={
                  process.env.PUBLIC_URL +
                  "/images/Icons/feature-icon/headphone.png"
                }
                alt=""
              />
              LISTEN BEFORE PURCHASE
            </h3>
            <p>
              Donec pede justo, fringilla vel, al, vulputate eget, arcu. In{" "}
            </p>
          </div>
          <div className="options">
            <h3>
              <img
                src={
                  process.env.PUBLIC_URL +
                  "/images/Icons/feature-icon/calendar.png"
                }
                alt=""
              />
              UPCOMING EVENTS
            </h3>
            <p>
              Donec pede justo, fringilla vel, al, vulputate eget, arcu. In{" "}
            </p>
          </div>
        </div>
      </div>
    </section>
    

    
      </div> 
)

    }
}