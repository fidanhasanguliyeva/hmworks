import React, { Component } from "react";
import ReactStars from "react-rating-stars-component";
export class ProductCard extends Component {
  ratingChanged = newRating => {
    console.log(newRating);
  };
  render() {
    const { src, title, artist, description, price, handleClick } = this.props;
    return (
      <div>
        <div className="about">
          <img src={src} alt="" />
          <div className="product-container">
            <div className="title">
              {title}
              <span>
                <i> {artist}</i>
              </span>
            </div>
            <ReactStars
              count={5}
              onChange={this.ratingChanged}
              size={24}
              color2={"#d23939"}
            />
          </div>
          <p className="product-container">{description}</p>
          <div className="price-cart-btn product-container">
         <span>${price}</span>  <button onClick={this.props.handleClick}>ADD TO CART</button>
        </div>
        </div>
        
      </div>
    );
  }
}
