import React, { useState, useEffect, Component } from "react";
import "../style.sass";
import { Header } from "./Header";
import { ListOfProducts } from "./ListOfProducts";




export class Main extends Component {
  constructor() {
    super();
    this.state = {
      
      isActive: false
    };
  }
  setActive = () => {
    this.setState({ isActive: true});
  };

 
  render() {
    return (
      <div>
        <Header />
      
        <ListOfProducts/>
        
      </div>
    );
  }
}
