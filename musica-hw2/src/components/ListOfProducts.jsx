import React, { Component } from "react";
import { ProductCard } from "./ProductCard";
import { ModalWindow } from "./ModalWindow";
import { getMusics } from "../API/fetchAPI";
export class ListOfProducts extends Component {
  constructor() {
    super();
    this.state = {
      data: {
        count: 0,
        musics: []
      },
      cart: [],
      isactive: false
    };
  }

  getdata = async () => {
    const answer = await getMusics();
    this.setState({ data: { musics: [...answer] } });
    console.log(this.state.data.musics);
  };

  componentDidMount() {
    this.getdata();
  }

  removeFromCart = (id, count) => {
    if (count > 1) {
      this.setState({
        cart: this.state.cart.map(item => {
          if (item.id === id) {
            return {
              ...item,
              count: item.count - 1
            };
          }
          return item;
        })
      });
    } else {
      this.setState({ cart: this.state.cart.filter(item => item.id !== id) });
    }
  };
  AddToCart = (imgPath, title, rate, cost, id) => {
    console.log("Add to cart begin");

    const isAvailable = this.state.cart.find(item => id === item.id);
    if (isAvailable) {
      this.setState({
        cart: this.state.cart.map(item => {
          if (item.id === id) {
            console.log(
              { ...item, count: item.count + 1 },
              "Inside if isavailable"
            );
            return { ...item, count: item.count + 1 };
          }
          return item;
        })
      });
    } else {
      this.setState({
        cart: [
          ...this.state.cart,
          {
            imgPath,
            title,
            rate,
            cost,
            id,
            count: 1
          }
        ]
      });
    }

    this.setState({ isactive: true });
  };

  render() {
    return (
      <section className="arrivals">
        <div className="container">
          <div className="arrivals-header">
            <h1>LATEST ARRIVALS IN MUSICA</h1>
            <img src={process.env.PUBLIC_URL + "/images/line.png"} alt="" />
            <div className="buttons">
              <button className="arrows-btn">
                <img
                  src={process.env.PUBLIC_URL + "/images/Icons/arrows/left.png"}
                  alt=""
                />
              </button>
              <button className="arrows-btn">
                <img
                  src={
                    process.env.PUBLIC_URL + "/images/Icons/arrows/right.png"
                  }
                  alt=""
                />
              </button>
            </div>
          </div>
          {this.state.isactive &&
            this.state.cart.map(
              ({ imgPath, title, rate, price, count, id }) => (
                <ModalWindow
                  key={id}
                  cart={this.state.cart}
                  src={process.env.PUBLIC_URL + { imgPath }}
                  rate={rate}
                  title={title}
                  cost={price}
                  count={count}
                  removeFromCart={this.removeFromCart}
                />
              )
            )}
          <ul className="product-list">
            {this.state.data.musics.map(
              ({ imgPath, id, title, rate, price, description, artist }) => (
                <li key={id} className="products-item">
                  <ProductCard
                    key={id}
                    isactive={this.props.activetab}
                    src={imgPath}
                    title={title}
                    artist={artist}
                    description={description}
                    price={price}
                    handleClick={() =>
                      this.AddToCart(imgPath, title, rate, price, id)
                    }
                  />
                </li>
              )
            )}
          </ul>
        </div>
      </section>
    );
  }
}
