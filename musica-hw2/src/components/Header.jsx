import React, { Component } from "react";
import {ModalWindow} from './ModalWindow'
import "../style.sass";

export class Header extends Component {
  constructor(){
    super();
  }
  render() {
    return (
      <header className="main-header">
        <div className="header-first-line ">
          <div className="container">
            <div className="header-container">
              <div className="icons">
                <img
                  src={process.env.PUBLIC_URL + "/images/Icons/facebook.png"}
                  alt="facebook"
                ></img>
                <img
                  src={process.env.PUBLIC_URL + "/images/Icons/dribble.png"}
                  alt="dribble"
                ></img>
                <img
                  src={process.env.PUBLIC_URL + "/images/Icons/twitter.png"}
                  alt="twitter"
                ></img>
                <img
                  src={process.env.PUBLIC_URL + "/images/Icons/mail.png"}
                  alt="mail"
                ></img>
                <img
                  src={process.env.PUBLIC_URL + "/images/Icons/vimeo.png"}
                  alt="vimeo"
                ></img>
              </div>
              <div className="first-line-login-cart">
                <a className="login-register">Login / Register</a>
                <button className="cart-btn">
                  <img
                    src={process.env.PUBLIC_URL + "/images/Icons/card.png"}
                  ></img>
                  Cart
                </button>
              </div>
            </div>
          </div>
        </div>
        <div className="container">
          <div className="header-second-line">
            <img src={process.env.PUBLIC_URL + "/images/logo.png"} alt="" />

            <ul className="header-nav">
              <li>
                <a href="">home</a>
              </li>
              <li>
                <a href="">cd's</a>
              </li>
              <li>
                <a href="">dvd's</a>
              </li>
              <li>
                <a href="">news</a>
              </li>
              <li>
                <a href="">portfolio</a>
              </li>
              <li>
                <a href="">contact us</a>
              </li>
            </ul>
          </div>
        </div>
        <section className="main-section">
          {/* <Carousel>
<h1>   <img src={process.env.PUBLIC_URL + '/images/SlideImages/main.png'} /></h1>
<h1>   <img src={process.env.PUBLIC_URL + '/images/SlideImages/main2.png'} /></h1>
<h1>   <img src={process.env.PUBLIC_URL + '/images/SlideImages/main3.png'} /></h1>
<h1>  <img src={process.env.PUBLIC_URL + '/images/SlideImages/main4.png'} /></h1>
<h1>  <img src={process.env.PUBLIC_URL + '/images/SlideImages/main5.png'} /></h1>
</Carousel> */}
        </section>
      </header>
    );
  }
}
