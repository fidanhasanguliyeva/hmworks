const navigationBar = document.getElementById("nav-bar");
let tabsContent = document.getElementById("tabContent");

navigationBar.addEventListener("click", TabOnClick);

for (let i = 0; i < tabsContent.children.length; i++) {
  navigationBar.children[i].dataset.index = i;
  if (i) tabsContent.children[i].hidden = true;
}

function TabOnClick(event) {
  let activeTabs = document.querySelectorAll(".active-ourservices");

  for (let i = 0; i < activeTabs.length; i++) {
    activeTabs[i].classList.remove("active-ourservices");
  }

  event.target.classList.add("active-ourservices");

  tabsContent.querySelector("li:not([hidden])").hidden = true;
  tabsContent.children[event.target.dataset.index].hidden = false;
}

const filtertitles = document.getElementById("filter-titles");
const projects = document.querySelectorAll(".project-item");

filtertitles.addEventListener("click", e => {
  if (e.target.classList.contains("filter-title")) {
    const title = e.target;
    const type = title.dataset.filterby || "project-item";
    const isActive = title.classList.contains("active");

    if (!isActive) {
      document.querySelector(".filter-title.active").classList.remove("active");
      title.classList.add("active");
      filterByClass(projects, type);
    }
    console.log(isActive);
  }
});

function filterByClass(elements, className) {
  for (let element of elements) {
    element.hidden = !element.classList.contains(className);
  }
}

const projectList = document.getElementById("projectList");
const loadBtn = document.querySelector(".load-more");


loadBtn.addEventListener("click", LoadMore);
for (let m = 0; m < projectList.children.length; m++) {
  if (m >= projectList.children.length / 3) {
    projectList.children[m].hidden = true;
  }
}


var clicks = 1;
loadBtn.onclick = function() {
  clicks++;
  //console.log(clicks);
};

function LoadMore() {
  if (clicks == 1) {
    for (let m = 0; m < 24; m++) {
      projectList.children[m].hidden = false;
    }
  }
  if (clicks == 2) {
    for (let m = 0; m < projectList.children.length; m++) {
      projectList.children[m].hidden = false;

    }
    loadBtn.style.display='none';
  }
  
}
